import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { createUI } from 'vue3-component-demo'
import Lego from 'lego-components'
// 加载样式
import 'lego-components/dist/lego-components.css'
import 'vue3-component-demo/dist/vue3-component-demo.css'

const a = createUI()

createApp(App).use(store).use(router).use(Lego).use(a).mount('#app')
